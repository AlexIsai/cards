import forma from "../module/classVisit.js";
import modal from "../module/classModal.js";
import  listCards from "../module/utils-functions.js";


class ChangeFormContent extends forma.ClassVisit {
    constructor(id) {
        super()
    }

    getContent(id) {
        let {
            title,
            getName,
            getSurName,
            choiceDoctor,
            getTarget,
            getDescription,
            getEmergency,
            buttonSend,
            form
        } = this.elements
        let changeContent = document.getElementsByClassName(`${id}`)
        let formChildren = changeContent[0].children
        let mainInfo = formChildren[0].children
        let dopInfo = formChildren[1].children
        let chooseDoctorValue = mainInfo[2].textContent.split(" ")
        let userName = mainInfo[1].textContent.split(" ")
        let targetValue = dopInfo[0].textContent.split('Цель визита: ', 2)
        let description = dopInfo[1].textContent.split('Описание: ')
        let emergency = dopInfo[2].textContent.split('Срочность: ')
        title.textContent = "Редактировать Карточку"
        choiceDoctor.value = chooseDoctorValue
        getName.value = userName[0]
        getSurName.value = userName[2]
        getTarget.value = targetValue[1]
        getDescription.value = description[1]
        getEmergency.value = emergency[1]
        if (choiceDoctor.value === "Dentist") {
            let dentist = new forma.VisitDentist
            dentist.renderSubsidiaryElement()
            let lastDate = dopInfo[3].textContent.split('Последний визит: ')
            dentist.elements.lastDate.value = lastDate[1]
        }
        if (choiceDoctor.value === "Therapist") {
            let therapist = new forma.Therapist
            therapist.renderSubsidiaryElement()
            let age = dopInfo[3].textContent.split('Возраст: ')
            therapist.elements.age.value = age[1]
        }
        if (choiceDoctor.value === "Cardiologist") {
            let cardiologist = new forma.Cardiologist
            let seek = dopInfo[4].textContent.split('Заболевания ССС: ')
            let pressure = dopInfo[3].textContent.split('Обычное давление: ')
            let age = dopInfo[5].textContent.split('Возраст: ')
            let IndexBody = dopInfo[6].textContent.split('Индекс массы тела: ')
            cardiologist.renderSubsidiaryElement()
            cardiologist.elements.pressure.value = pressure[1]
            cardiologist.elements.sick.value = seek[1];
            cardiologist.elements.age.value = age[1]
            cardiologist.elements.imb.value = IndexBody[1]
        }
        super.validation()
        buttonSend.addEventListener('click', e => {
            e.preventDefault()
            let href = `https://ajax.test-danit.com/api/v2/cards/${id}`
            if (choiceDoctor.value === "Therapist") {
                const formChildrenArr = document.querySelectorAll('.input__form, .textarea__form, .form__select')
                const forSend = {...formChildrenArr}
                let dopInfo = {age: formChildrenArr[6].value}

                if (this.elements.flag === true) {
                    super.send(href, "PUT", forSend, dopInfo)
                    form.remove()
                    const mdw = document.querySelector('.modal-container')
                    mdw.remove()
                    setTimeout(listCards.rediReload, 100)
                }
            }
            if (choiceDoctor.value === "Cardiologist") {
                const formChildrenArr = document.querySelectorAll('.input__form, .textarea__form, .form__select')
                const forSend = {...formChildrenArr}
                let dopInfo = {
                    pressure: forSend[6].value,
                    indexBody: forSend[7].value,
                    seek: forSend[8].value,
                    age: forSend[9].value
                }
                if (this.elements.flag === true) {
                    super.send(href, "PUT", forSend, dopInfo)
                    form.remove()
                    const mdw = document.querySelector('.modal-container')
                    mdw.remove()
                    setTimeout(listCards.rediReload, 100)
                }

            }
            if (choiceDoctor.value === "Dentist") {
                const formChildrenArr = document.querySelectorAll('.input__form, .form__select, .textarea__form')
                const forSend = {...formChildrenArr}
                console.log(forSend);
                let dopInfo = {lastDate: formChildrenArr[6].value}
                if (this.elements.flag === true) {
                    super.send(href, "PUT", forSend, dopInfo)
                    form.remove()
                    const mdw = document.querySelector('.modal-container')
                    mdw.remove()
                    setTimeout(listCards.rediReload, 100)
                    console.log(form);
                }
            }
        })
        super.getParentOptions()
        this.elements.title.textContent = "Редактировать карточку"
    }

    render(id) {
        let idItem = id
        let {
            title,
            getName,
            getSurName,
            choiceDoctor,
            getTarget,
            getDescription,
            getEmergency,
            buttonSend,
            form,
            buttonClose,
            error
        } = this.elements
        let containerChange = document.querySelector('.modal-container')
        form.classList.add('form-standard')
        form.append(title, buttonClose, choiceDoctor, getName, getSurName, getTarget, getEmergency, getDescription, buttonSend, error)
        containerChange.append(form)
        super.allElementsGetStyle()
        super.allElementsGetContent()
        super.renderDopInfo()
        this.getContent(idItem)
    }
}


export default {
    ChangeFormContent
}