import ClassForm from "../module/ClassForm.js";
import listCards from "../module/utils-functions.js"

class ClassVisit extends ClassForm.ClassForm {
    constructor() {
        super();
        this.elements = {
            ...this.elements,
            getName: document.createElement('input'),
            getSurName: document.createElement('input'),
            getTarget: document.createElement('input'),
            getDescription: document.createElement('textarea'),
            getEmergency: document.createElement('select'),
            choiceDoctor: document.createElement('select'),
            dopInfo: document.createElement('div'),
            buttonSend: document.createElement('button'),
            error: document.createElement('span'),
            flag: true
        }
    }

    allElementsGetContent() {
        let {
            title,
            getName,
            getSurName,
            choiceDoctor,
            getTarget,
            getDescription,
            getEmergency,
            buttonSend,
            error,
        } = this.elements
        getName.placeholder = "Enter your name";
        getSurName.placeholder = "Enter your surname"
        choiceDoctor.innerHTML = `<option data-name ="choose a doctor"> Choose a doctor</option><option data-name ="cardiologist">Cardiologist</option><option data-name ="dentist">Dentist</option><option data-name ="therapist"> Therapist</option>`;
        getTarget.placeholder = 'Enter your target';
        getDescription.placeholder = 'Enter description'
        getEmergency.innerHTML = `<option data-name ="chooseEmergence"> Choose emergence</option><option data-name ="high">High</option><option data-name ="middle">Middle</option><option data-name ="low">Low</option>`
        buttonSend.textContent = "send"

    }

    getParentOptions() {
        super.elementsGetContent()
        super.closeForm()
    }

    allElementsGetStyle() {
        let {
            getName,
            getSurName,
            choiceDoctor,
            getTarget,
            getDescription,
            getEmergency,
            dopInfo,
            buttonSend,
            error,
            form
        } = this.elements
        getName.classList.add('input__form');
        getSurName.classList.add('input__form');
        getEmergency.classList.add('form__select');
        getTarget.classList.add('input__form');
        getDescription.classList.add('textarea__form');
        choiceDoctor.classList.add('form__select');
        dopInfo.classList.add('form__more-info');
        buttonSend.classList.add("btn-send");
        error.classList.add('form__error')
        form.classList.add('form-standard')
    }

    removeDopInfo() {
        let form = document.querySelector('.form-standard');
        let formChildren = form.childNodes
        formChildren.forEach(item => {
            if (item.classList.contains('form__more-info') || item.classList.contains('form__error')) {
                item.remove()
            }
        })
    }

    handler() {
        let {form} = this.elements
        this.renderDopInfo()
        this.validation()
        this.sendCards()
        let modal = document.querySelector('.modal-container')
        modal.addEventListener('click', e => {
            let cursorTarget = e.target
            if (cursorTarget.classList.contains('modal-container')) {
                this.formRemove()
            }
        })
    }

    renderDopInfo() {
        let {
            choiceDoctor
        } = this.elements
        choiceDoctor.addEventListener('change', e => {
            let cardiologistic = new Cardiologist
            let dentist = new VisitDentist
            let therapist = new Therapist
            if (choiceDoctor.value === "Cardiologist") {
                cardiologistic.renderSubsidiaryElement()
            }
            if (choiceDoctor.value === "Dentist") {
                dentist.renderSubsidiaryElement()
            }
            if (choiceDoctor.value === "Therapist") {
                therapist.renderSubsidiaryElement()
            }
            if (choiceDoctor.value === "Choose a doctor") {
                this.removeDopInfo()
            }
        })
    }

    formRemove() {
        let {
            getName,
            getSurName,
            choiceDoctor,
            getTarget,
            getDescription,
            getEmergency,
            dopInfo,
            form
        } = this.elements
        let formChildren = [getName, getSurName, choiceDoctor, getTarget, getDescription, getEmergency, dopInfo,]
        formChildren.forEach(item => item.value = "")
        form.remove()
    }

    validation() {
        const {
            buttonSend,
            error,
            form,
        } = this.elements
        buttonSend.addEventListener('click', e => {
                e.preventDefault()
                let moreInfo = document.querySelectorAll(".input__form, .textarea__form")
                form.insertAdjacentElement('beforeend', error)
                for (let some of moreInfo) {
                    if (some.value.length <= 0) {
                        console.log(some);
                        this.elements.flag = false
                        return error.textContent = "заполните все поля"
                    } else {
                        this.elements.flag = true
                        error.textContent = "send"
                    }
                }
                console.log(this.elements.choiceDoctor.value);
                if (this.elements.choiceDoctor.value === "Choose a doctor") {
                    this.elements.flag = false
                    error.textContent = 'выберете врача'
                } else if (this.elements.getEmergency.value === 'Choose emergence') {
                    this.elements.flag = false
                    error.textContent = 'выберете срочность'
                }
                console.log(this.elements.flag);
            }
        )
    }

    sendCards() {
        let dentist = new VisitDentist
        let therapist = new Therapist
        let cardiologist = new Cardiologist
        const {
            buttonSend,
            error,
            choiceDoctor
        } = this.elements
        buttonSend.addEventListener('click', e => {
            if (error.textContent === "send" && choiceDoctor.value === "Dentist") {
                dentist.sendSome()
                this.formRemove()
                const mdw = document.querySelector('.modal-container')
                mdw.remove()
setTimeout(listCards.rediReload, 100)

            }
            if (error.textContent === "send" && choiceDoctor.value === "Therapist") {
                therapist.sendSome()
                this.formRemove()
                const mdw = document.querySelector('.modal-container')
                mdw.remove()
                setTimeout(listCards.rediReload, 100)

            }
            if (error.textContent === "send" && choiceDoctor.value === "Cardiologist") {
                cardiologist.sendSome()
                this.formRemove()
                const mdw = document.querySelector('.modal-container')
                mdw.remove()
                setTimeout(listCards.rediReload, 100)

            }
        })
    }

    send(link, some, someObject, optionObject) {
        let {
            getName,
            getSurName,
            choiceDoctor,
            getTarget,
            getDescription,
            getEmergency,
            dopInfo,
            buttonSend,
            error,
            form
        } = this.elements
        fetch(link, {
            method: `${some}`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${'b72f0991-4578-4644-9fb3-a034dde491d8'}`
            },
            body: JSON.stringify({
                ...optionObject,
                title: `визит к ${someObject[0].value}`,
                doctor: someObject[0].value,
                name: `${someObject[1].value}  ${someObject[2].value}`,
                target: someObject[3].value,
                emergency: someObject[4].value,
                description: someObject[5].value,
            })
        })
            .then(response => response.json())
            .then(response => console.log(response))
    }


    renderStandardItems() {
        // this.render()
        this.allElementsGetContent()
        this.allElementsGetStyle()
        this.getParentOptions()
        let {
            title,
            getName,
            getSurName,
            choiceDoctor,
            getTarget,
            getDescription,
            getEmergency,
            form,
            buttonSend,
            buttonClose,
            error
        } = this.elements
        form.append(title, buttonClose, choiceDoctor, getName, getSurName, getTarget, getEmergency, getDescription, buttonSend, error)
        let modalContainer = document.querySelector(".modal-container")
        modalContainer.append(form)
        this.handler()
        this.removeDopInfo()

    }
}


/////////////////////////////////VisitDentist//////////////////////////////////////////////////////////


class VisitDentist extends ClassVisit {
    constructor() {
        super();
        this.elements = {
            ...this.elements,
            lastDate: document.createElement('input'),
        }
    }

    allElementsGetStyle() {
        let {
            lastDate,
            dopInfo,
        } = this.elements
        lastDate.classList.add('input__form');
        dopInfo.classList.add('form__more-info');
        this.allElementsGetContent()

    }

    allElementsGetContent() {
        let {
            lastDate,
        } = this.elements

        lastDate.placeholder = "Enter your last visit date";
    }

    sendSome() {
        const formChildrenArr = document.querySelectorAll('.input__form, .textarea__form, .form__select')
        const forSend = {...formChildrenArr}
        console.log(forSend);
        let dopInfo = {lastDate: formChildrenArr[6].value}
        super.send("https://ajax.test-danit.com/api/v2/cards", "POST", forSend, dopInfo)
    }

    renderSubsidiaryElement() {
        super.removeDopInfo()
        this.allElementsGetStyle()
        let {
            lastDate,
            form,
            dopInfo,
        } = this.elements
        dopInfo.append(lastDate)
        form = document.querySelector('.btn-send')
        form.insertAdjacentElement('beforebegin', dopInfo)
    }
}


/////////////////////////////////////////////Cardiologist////////////////////////////////////////////////////

class Cardiologist extends ClassVisit {
    constructor() {
        super();
        this.elements = {
            ...this.elements,
            age: document.createElement("input"),
            pressure: document.createElement("input"),
            imb: document.createElement('input'),
            sick: document.createElement('textarea')
        }
    }

    allElementsGetStyle() {
        let {
            pressure,
            imb,
            sick,
            dopInfo,
            age,
        } = this.elements
        pressure.classList.add('input__form');
        imb.classList.add('input__form');
        sick.classList.add('textarea__form')
        dopInfo.classList.add('form__more-info')
        age.classList.add('input__form')
        this.allElementsGetContent()
    }

    allElementsGetContent() {
        let {
            pressure,
            imb,
            sick,
            age,
        } = this.elements
        pressure.placeholder = "Enter your pressure";
        imb.placeholder = "Enter your imb";
        sick.placeholder = "describe your sick";
        age.placeholder = "enter your age"
    }

    sendSome() {
        const formChildrenArr = document.querySelectorAll('.input__form, .textarea__form, .form__select')
        const forSend = {...formChildrenArr}
        let dopInfo = {
            pressure: forSend[6].value,
            indexBody: forSend[7].value,
            seek: forSend[8].value,
            age: forSend[9].value
        }
        super.send("https://ajax.test-danit.com/api/v2/cards", "POST", forSend, dopInfo)

    }


    renderSubsidiaryElement() {
        super.removeDopInfo()
        this.allElementsGetStyle()
        let {
            age,
            pressure,
            imb,
            sick,
            form,
            dopInfo,

        } = this.elements
        dopInfo.append(pressure, imb, sick, age)
        form = document.querySelector('.btn-send')
        form.insertAdjacentElement('beforebegin', dopInfo)
    }
}

//////////////////////////////////////////Therapist//////////////////////////////////////////////////////////////

class Therapist extends ClassVisit {
    constructor() {
        super();
        this.elements = {
            ...this.elements,
            dopInfo: document.createElement("div"),
            age: document.createElement('input'),
        }
    }

    allElementsGetStyle() {
        let {
            age,
            dopInfo
        } = this.elements
        age.classList.add('input__form');
        dopInfo.classList.add('form__more-info')
        this.allElementsGetContent()
    }

    allElementsGetContent() {
        let {age,} = this.elements
        age.placeholder = "Enter your age";
    }

    sendSome() {
        const formChildrenArr = document.querySelectorAll('.input__form, .textarea__form, .form__select')
        const forSend = {...formChildrenArr}
        let dopInfo = {age: formChildrenArr[6].value}
        super.send("https://ajax.test-danit.com/api/v2/cards", "POST", forSend, dopInfo)

    }

    renderSubsidiaryElement() {
        super.removeDopInfo()
        this.allElementsGetStyle()
        let {
            age,
            form,
            dopInfo,
        } = this.elements
        dopInfo.append(age,)
        form = document.querySelector('.btn-send')
        form.insertAdjacentElement('beforebegin', dopInfo)
    }
}


export default {
    ClassVisit,
    VisitDentist,
    Cardiologist,
    Therapist,
}
