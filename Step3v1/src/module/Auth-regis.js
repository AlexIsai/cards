import form from "../module/ClassForm.js";
import api from "../module/API.js";


class Athorization extends form.ClassForm {
    constructor() {
        super();
        this.elements = {
            ...this.elements,
            userName: document.createElement('input'),
            userPass: document.createElement('input'),
            error: document.createElement('p'),
            btnSend: document.createElement('button')
        }
    }

    render() {
        let {title, userName, userPass, error, form, btnSend, buttonClose} = this.elements
        form.append(title, userName, userPass, error, btnSend, buttonClose)
        let modal = document.querySelector('.modal-container')
        modal.append(form)
        this.getContent()
        this.getStyles()
        this.handler()
        this.formRemove()
    }

    getContent() {
        let {userName, userPass, btnSend} = this.elements
        userName.placeholder = "Логин"
        userPass.placeholder = "Пароль"
        btnSend.textContent = 'Авторизироваться'

    }

    getStyles() {
        let {userName, userPass,btnSend} = this.elements
        userName.type = "text";
        userName.classList.add('input__form')
        userPass.type = 'password'
        userPass.classList.add('input__form')
        btnSend.classList.add("btn-aut")
        super.elementsGetContent()
        super.closeForm()
        this.elements.title.textContent = "Авторизация"
    }

    formRemove() {
        let {form} = this.elements
        let modal = document.querySelector('.modal-container')
        modal.addEventListener('click', e => {
            let cursorTarget = e.target
            if (cursorTarget.classList.contains(".modal-container")) {
                let formChildren = document.querySelectorAll('.form__input')
                formChildren.forEach(item => item.value = "")
                form.remove()
                modal.remove()
            }
        })
    }

    handler() {
        let {userName, userPass, btnSend, error, form} = this.elements
        btnSend.addEventListener('click', async (e) => {
            let modal = document.querySelector('.modal-container')
            e.preventDefault()
            const token = await api.auth({
                email: `${userName.value}`,
                password: `${userPass.value}`
            }).then(r => r.text());

            if (token !== "Incorrect username or password") {

                localStorage.setItem('token', token);

                const cards = await api.getCards();
                let cardList = document.querySelector('#app')
                cardList.style.display = "block"
                let btnCreate = document.querySelector('.getCards')
                btnCreate.style.display = "inline"
                let authBtn = document.querySelector('#authorization')
                authBtn.style.display = "none"
                form.remove()
                modal.remove()
            } else {
                error.textContent = 'Неверный логин или пароль!';
                error.style.color = 'red';

            }
        })
    }

}


export default {
    Athorization,
}

